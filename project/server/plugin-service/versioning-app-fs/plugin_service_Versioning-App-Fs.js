
'use strict';

const DatabaseConst = require('z-abs-funclayer-database-server/server/core/database-const');
const DataDatabase = require('z-abs-funclayer-database-server/server/core/database-database');
const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const PluginService = require('z-abs-corelayer-server/server/service/plugin-service');
const DataResponse = require('z-abs-corelayer-server/server/data-response');
const ChildProcess = require('child_process');


class VersioningAppFs extends PluginService {
  constructor() {
    super(PluginService.OFFER_LOCAL);
    this.priority = 10;
    this.git = null;
    this.nodejs = null;
    this.npm = null;
    this.resultingServerDatas = [];
    this.dbName = 'z-database-status';
    this.releaseData = Reflect.get(global, 'release-data@abstractor');
    this.dbType = DatabaseConst.TYPE_GIT;
    this.loadingDb = false;
  }
  
  onInit() {
    if('delivery' === this.releaseData.releaseStep) {
      this.dbType = DatabaseConst.TYPE_NPM;
    }
    else {
      this.dbType = DatabaseConst.TYPE_GIT;
    }
    this.subscribeOnNodeStarted(() => {
      ddb.writelnTime(ddb.green('          * '), this.releaseData.appName, ' ', ddb.yellow(this.releaseData.packageJson.version), ddb.green(' on'), ' node ', ddb.yellow(this.nodejs), ddb.green(' and'), ' npm ', ddb.yellow(this.npm));
      ddb.writelnTime(ddb.green('          * '), 'database ', ddb.yellow(this.dbName), ' ', this._dbTypeAsText());
    });
    this._load((err) => {
      if(err) {
        ddb.error('VersioningAppFs load error: ', err)
      }
      this._publish();
    });
    this.done();
  }
  
  onExit(hasException) {
    this.done();
  }
  
  onSubscribe() {
    this._publish();
  }
  
  _publish() {
    this.publish('ToolVersionsGet', (err) => {
      if(err) {
        ddb.error('ToolVersionsGet: ', err);
      }
    }, this.git, this.nodejs, this.npm);
  }
  
  _load(cb) {
    let pendings = 0;
    ++pendings;
    this._getNpmVersions(() => {
      if(0 === --pendings) {
        cb();
      }
    });
    ++pendings;
    this._getGitVersion(() => {
      if(0 === --pendings) {
        cb();
      }
    });
    if(DataDatabase) {
      if(!this.loadingDb) {
        ++pendings;
        this.loadingDb = true;
        DataDatabase.load(this.dbName, true, this.dbType, this.releaseData, (err) => {
          this.loadingDb = false;
          if(err) {
            cb(err);
          }
          else {
            if(0 === --pendings) {
              cb();
            }
          }
        });
      }
    }
    const serverDatas = DataResponse._.serverDatas.wsWebServers;
    serverDatas.forEach((serverData) => {
      this.resultingServerDatas.push({
        name: serverData.name,
        type: serverData.type,
        host: serverData.host,
        port: serverData.port
      });
    });
  }
  
  getWsServers() {
    return this.resultingServerDatas;
  }
  
  getGit() {
    return this.git;
  }
  
  getNodejs() {
    return this.nodejs;
  }
  
  getNpm() {
    return this.npm;
  }
  
  _getNpmVersions(cb) {
    ChildProcess.exec('npm --versions', {cwd: ActorPath.getActorPath()}, (err, stdout) => {
      if(!err) {
        this.nodejs = this._getNpmVersionParameter('node', stdout);
        this.npm = this._getNpmVersionParameter('npm', stdout);
      }
      else {
        ddb.error('NPM ERROR: ', err);
        this.nodejs = null;
        this.npm = null;
      }
      if(cb) {
        cb();
      }
    });
  }
  
  _getGitVersion(cb) {
    ChildProcess.exec('git --version', {cwd: ActorPath.getActorPath()}, (err, stdout) => {
      if(!err) {
        this.git = stdout;
      }
      else {
        ddb.error('GIT ERROR: ', err);
        this.git = null;
      }
      if(cb) {
        cb();
      }
    });
  }
  
  _getNpmVersionParameter(name, text) {
    const paramIndex = text.indexOf(name);
    if(-1 !== paramIndex) {
      const start = text.indexOf(': ', paramIndex);
      const stop = text.indexOf(',' , paramIndex);
      if(-1 !== start && -1 !== stop && stop > start) {
        return text.substring(start + 3, stop - 1);
      }
    }
    return null;
  }
  
  _dbTypeAsText() {
    switch(this.dbType) {
      case DatabaseConst.TYPE_NPM:
        return '(npm)'
      case DatabaseConst.TYPE_GIT:
        return '(git)'
      default:
        return '(unknown)'
    }
  }
}


module.exports = VersioningAppFs;
