
'use strict';

const OsHardware = require('./os-hardware');
const DataDatabase = require('z-abs-funclayer-database-server/server/core/database-database');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ToolVersionsGet extends PluginBaseMulti {
  static DB_NAME = 'z-database-status';
  
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(git, nodejs, npm) {
    const releaseData = Reflect.get(global, 'release-data@abstractor');
    this.expectAsynchResponseTemp();
    const indexStatusOsDevelopment = this.expectAsynchResponse();
    ToolVersionsGet.getStatusOsDevelopment((err, value) => {
      if(!err) {
        this.asynchResponseSuccess(indexStatusOsDevelopment, value);
      }
      else {
        this.asynchResponseError(err.message, indexStatusOsDevelopment);
      }
    });
    const indexStatusTools = this.expectAsynchResponse();
    ToolVersionsGet.getStatusActorjsTool(releaseData.packageJson.folder, releaseData.packageJson.version, (err, value) => {
      if(!err) {
        return this.asynchResponseSuccess(indexStatusTools, value);
      }
      else {
        this.asynchResponseError(err.message, indexStatusTools);
      }
    });
  
    const indexGit = this.expectAsynchResponse();
    if(git) {
      const versionMatch = git.match(/\d+\.\d+\.\d+/);
      ToolVersionsGet.getStatusExternalTool('git', versionMatch[0], (err, value) => {
        if(!err) {
          this.asynchResponseSuccess(indexGit, value);
        }
        else {
          this.asynchResponseError(err.message, indexGit);
        }
      });
    }
    else {
      process.nextTick(() => {
        this.asynchResponseSuccess(indexGit, new Error('Could not get git.'), {
          value: '',
          status: 4
        });
      });
    }
    const indexNodejs = this.expectAsynchResponse();
    if(nodejs) {
      const versionMatch = nodejs.match(/\d+\.\d+\.\d+/);
      ToolVersionsGet.getStatusExternalTool('nodejs', versionMatch[0], (err, value) => {
        if(!err) {
          this.asynchResponseSuccess(indexNodejs, value);
        }
        else {
          this.asynchResponseError(err.message, indexNodejs);
        }
      });
    }
    else {
      process.nextTick(() => {
        this.asynchResponseSuccess(indexNodejs, new Error('Could not get nodejs.'), {
          value: '',
          status: 4
        });
      });
    }
    const indexNpm = this.expectAsynchResponse();
    if(npm) {
      const versionMatch = npm.match(/\d+\.\d+\.\d+/);
      ToolVersionsGet.getStatusExternalTool('npm', versionMatch[0], (err, value) => {
        if(!err) {
          this.asynchResponseSuccess(indexNpm, value);
        }
        else {
          this.asynchResponseError(err.message, indexNpm);
        }
      });
    }
    else {
      process.nextTick(() => {
        this.asynchResponseSuccess(indexNpm, new Error('Could not get npm.'), {
          value: '',
          status: 4
        });
      });
    }
    this.unExpectAsynchResponseTemp();
  }
  
  static getStatusOsDevelopment(cb) {
    OsHardware.getDevEnvironment((statusOsDevelopment) => {
      const data = {
        version: this._getOsDevelopmentVersion(statusOsDevelopment),
        distribution: this._getDistribution(statusOsDevelopment),
        platform: statusOsDevelopment.platform,
        arch: statusOsDevelopment.arch,
        verification: 2
      };
      DataDatabase.getRow(ToolVersionsGet.DB_NAME, 'status-os-development', 'key', [data.version, data.distribution, data.platform, data.arch], (err, row) => {
        if(row) {
          data.verification = row[4];
        }
        else if(err) {
          data.verification = 6;
        }
        cb(null, data);
      });
    });
  }
  
  static getStatusActorjsTool(name, version, cb) {
    const index = version.indexOf('+');
    const buildVersion = -1 !== index;
    const lookupVersion = buildVersion ? version.substring(0, index) : version;
    DataDatabase.getRow(ToolVersionsGet.DB_NAME, 'status-abstraktor-tool', 'name_version', [name, lookupVersion], (err, row) => {
      const data = {
        name: name,
        version: version,
        verification: 2
      };
      if(row) {
        data.verification = buildVersion && 1 === row[2] ? 2 : row[2];
      }
      else if(err) {
        data.verification = 6;
      }
      cb(null, data);
    });
  }
  
  static prepareVersion(version) {
    const versions = version.split('.');
    versions[0] = versions[0].padStart(4, '0');
    versions[1] = versions[1].padStart(4, '0');
    versions[2] = versions[2].padStart(4, '0');
    return versions.join('');
  }
  
  static getStatusExternalTool(name, version, cb) {
    const versionC = ToolVersionsGet.prepareVersion(version);
    const index = version.indexOf('+');
    const buildVersion = -1 !== index;
    const lookupVersion = buildVersion ? version.substring(0, index) : version;
    DataDatabase.getRows(ToolVersionsGet.DB_NAME, 'status-external-tools-range', ['name'], [name], (row) => {
      const versionFrom = ToolVersionsGet.prepareVersion(row[1]);
      const versionTo = ToolVersionsGet.prepareVersion(row[2]);
      return versionC >= versionFrom && versionC <= versionTo;
    }, (err, row) => {
      const data = {
        value: version,
        status: row ? row[0][3] : 5
      };
      cb(null, data);
    });
  }
  
  static _getDistribution(statusOsDevelopment) {
    if('Linux' === statusOsDevelopment.type) {
      if(statusOsDevelopment.distributionData?.dist && statusOsDevelopment.distributionData?.release) {
        return `${statusOsDevelopment.distributionData.dist} ${statusOsDevelopment.distributionData.release}`;
      }
      else {
        return '';
      }
    }
    return statusOsDevelopment.version;
  }
  
  static _getOsDevelopmentVersion(statusOsDevelopment) {
    if('Windows_NT' === statusOsDevelopment.type) {
      const index = statusOsDevelopment.versionfromNode.lastIndexOf(' ');
      if(-1 !== index) {
        return statusOsDevelopment.versionfromNode.substring(index + 1);
      }
      else {
        return '';
      }
    }
    else if('Linux' === statusOsDevelopment.type) {
      const index = statusOsDevelopment.release.indexOf('WSL');
      if(-1 !== index) {
        return `${statusOsDevelopment.version} (${statusOsDevelopment.release.substring(index)})`;
      }
      else {
        return statusOsDevelopment.version;
      }
    }
    else if('Darwin' === statusOsDevelopment.type) {
      return `${statusOsDevelopment.type} ${statusOsDevelopment.release}`;
    }
  }
}


module.exports = ToolVersionsGet;
